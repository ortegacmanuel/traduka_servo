module Billing
  class OnSetBillingAddressOfTranslationRequest

    def initialize(event_store)
      @repository = AggregateRootRepository.new(event_store)
    end

    def call(command)
      @repository.with_aggregate(TranslationRequest, command.aggregate_id) do |translation_request|
        translation_request.set_billing_address(
          command.line1,
          command.line2,
          command.city,
          command.postal_code,
          command.state,
          command.country,
          command.set_as_default
        )
      end
    end
  end
end