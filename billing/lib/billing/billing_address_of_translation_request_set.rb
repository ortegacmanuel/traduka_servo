module Billing
  class BillingAddressOfTranslationRequestSet < Event
    attribute :translation_request_id, Types::UUID
    attribute :line1, Types::String
    attribute :line2, Types::String.optional
    attribute :city, Types::String
    attribute :postal_code, Types::String.optional
    attribute :state, Types::String.optional
    attribute :country, Types::String
    attribute :set_as_default, Types::Bool
  end
end