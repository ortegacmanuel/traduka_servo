module Billing
  class TranslationRequest
    include AggregateRoot

    def initialize(id)
      @id = id
    end

    def set_billing_address(line1, line2, city, postal_code, state, country, set_as_default)
      apply BillingAddressOfTranslationRequestSet.new(data: {
        translation_request_id: @id,
        line1: line1,
        line2: line2,
        city: city,
        postal_code: postal_code,
        state: state,
        country: country,
        set_as_default: set_as_default
      })
    end

    on BillingAddressOfTranslationRequestSet do |event|
      @line1 = event.data[:line1]
      @line2 = event.data[:line2]
      @city = event.data[:city]
      @postal_code = event.data[:postal_code]
      @state = event.data[:state]
      @country = event.data[:country]
    end
  end
end