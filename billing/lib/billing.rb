module Billing
  class Configuration
    def initialize; end

    def call(bus, event_store)
      bus.register(SetBillingAddressOfTranslationRequest, OnSetBillingAddressOfTranslationRequest.new(event_store))
    end
  end
end
