module Requesting
  class Configuration
    def initialize(word_counter)
      @word_counter = word_counter
    end

    def call(bus, event_store)
      bus.register(SubmitTranslationRequest, OnSubmitTranslationRequest.new(event_store, @word_counter))
      bus.register(AcceptTranslationRequest, OnAcceptTranslationRequest.new(event_store))
    end
  end
end
