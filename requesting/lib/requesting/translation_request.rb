module Requesting
  class TranslationRequest
    include AggregateRoot

    SourceAndTargetLanguageCannotBeTheSame = Class.new(StandardError)
    MaxNumberOfWordsExceeded = Class.new(StandardError)

    def initialize(id)
      @id = id
      @state = :draft
    end

    def submit(translation_requester_id, source_languague, target_languague, translation_text)
      raise SourceAndTargetLanguageCannotBeTheSame if source_languague == target_languague
      raise MaxNumberOfWordsExceeded if translation_text.word_count > 300

      apply TranslationRequestSubmitted.new(data: {
        translation_request_id: @id,
        translation_requester_id: translation_requester_id,
        source_languague: source_languague,
        target_languague: target_languague,
        translation_text: translation_text
      })
    end

    def accept
      apply TranslationRequestAccepted.new(data: {
        translation_request_id: @id,
      })
    end

    on TranslationRequestSubmitted do |event|
      @translation_requester_id = event.data[:translation_requester_id]
      @source_languague = event.data[:source_languague]
      @target_languague = event.data[:target_languague]
      @translation_text = event.data[:translation_text]
      @state = :submitted
    end

    on TranslationRequestAccepted do |event|
      @state = :accepted
    end
  end
end