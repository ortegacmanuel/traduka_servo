module Requesting
  class OnAcceptTranslationRequest

    def initialize(event_store)
      @repository = AggregateRootRepository.new(event_store)
    end

    def call(command)
      @repository.with_aggregate(TranslationRequest, command.aggregate_id) do |translation_request|
        translation_request.accept
      end
    end
  end
end