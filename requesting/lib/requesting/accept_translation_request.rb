module Requesting
  class AcceptTranslationRequest < Command
    attribute :translation_request_id, Types::UUID

    alias :aggregate_id :translation_request_id
  end
end