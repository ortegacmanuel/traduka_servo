module Requesting
  class OnSubmitTranslationRequest

    def initialize(event_store, word_counter)
      @repository = AggregateRootRepository.new(event_store)
      @word_counter = word_counter
    end

    def call(command)
      @repository.with_aggregate(TranslationRequest, command.aggregate_id) do |translation_request|
        translation_text = Types::TranslationText.new(
          raw: command.translation_text.raw,
          word_count: @word_counter.new(command.translation_text.raw).word_count
        )

        translation_request.submit(
          command.translation_requester_id,
          command.source_languague,
          command.target_languague,
          translation_text
        )
      end
    end
  end
end