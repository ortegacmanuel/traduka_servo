module Requesting
  class TranslationRequestSubmitted < Event
    attribute :translation_request_id, Types::UUID
    attribute :translation_requester_id, Types::UUID
    attribute :source_languague, Types::Strict::String
    attribute :target_languague, Types::Strict::String
    attribute :translation_text, Types::TranslationText
  end
end