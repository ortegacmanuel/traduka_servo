module Requesting
  class TranslationRequestAccepted < Event
    attribute :translation_request_id, Types::UUID
  end
end