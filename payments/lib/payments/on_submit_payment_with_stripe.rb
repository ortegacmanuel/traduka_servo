module Payments
  class OnSubmitPaymentWithStripe

    def initialize(event_store)
      @repository = AggregateRootRepository.new(event_store)
    end

    def call(command)
      @repository.with_aggregate(Payment, command.aggregate_id) do |payment|
        payment.submit_with_stripe(
          command.translation_request_id,
          command.payment_method_id,
          command.amount,
          command.customer_ip,
          command.customer_user_agent
        )
      end
    end
  end
end