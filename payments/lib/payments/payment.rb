module Payments
  class Payment
    include AggregateRoot

    def initialize(id)
      @id = id
    end

    def submit_with_stripe(translation_request_id, payment_method_id, amount, customer_ip, customer_user_agent)
      apply PaymentWithStripeSubmitted.new(data: {
        payment_id: @id,
        translation_request_id: translation_request_id,
        payment_method_id: payment_method_id,
        amount: amount,
        customer_ip: customer_ip,
        customer_user_agent: customer_user_agent
      })
    end

    def mark_as_succeeded_with_stripe(payment_intent_id, client_secret)
      apply PaymentWithStripeSucceeded.new(data: {
        payment_id: @id,
        payment_intent_id: payment_intent_id,
        client_secret: client_secret
      })
    end

    on PaymentWithStripeSubmitted do |event|
      @amount = event.data[:amount]
      @translation_request_id = event.data[:translation_request_id]
      @customer_ip = event.data[:customer_ip]
      @customer_user_agent = event.data[:customer_user_agent]
    end

    on PaymentWithStripeSucceeded do |event|
      @payment_intent_id = event.data[:payment_intent_id]
      @client_secret = event.data[:client_secret]
    end
  end
end