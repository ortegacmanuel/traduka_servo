module Payments
  class OnMarkPaymentWithStripeAsSucceeded

    def initialize(event_store)
      @repository = AggregateRootRepository.new(event_store)
    end

    def call(command)
      @repository.with_aggregate(Payment, command.aggregate_id) do |payment|
        payment.mark_as_succeeded_with_stripe(
          command.payment_intent_id,
          command.client_secret
        )
      end
    end
  end
end