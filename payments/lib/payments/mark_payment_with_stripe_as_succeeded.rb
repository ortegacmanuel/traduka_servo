module Payments
  class MarkPaymentWithStripeAsSucceeded < Command
    attribute :payment_id, Types::UUID
    attribute :payment_intent_id, Types::String
    attribute :client_secret, Types::String

    alias :aggregate_id :payment_id
  end
end