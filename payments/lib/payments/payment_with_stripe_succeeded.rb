module Payments
  class PaymentWithStripeSucceeded < Event
    attribute :payment_id, Types::UUID
    attribute :payment_intent_id, Types::String
    attribute :client_secret, Types::String
  end
end