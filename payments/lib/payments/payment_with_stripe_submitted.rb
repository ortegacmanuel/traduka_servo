module Payments
  class PaymentWithStripeSubmitted < Event
    attribute :payment_id, Types::UUID
    attribute :translation_request_id, Types::UUID
    attribute :payment_method_id, Types::String
    attribute :amount, Types::Float
    attribute :customer_ip, Types::String
    attribute :customer_user_agent, Types::String
  end
end