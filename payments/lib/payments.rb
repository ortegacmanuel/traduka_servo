module Payments
  class Configuration
    def initialize; end

    def call(bus, event_store)
      bus.register(SubmitPaymentWithStripe, OnSubmitPaymentWithStripe.new(event_store))
      bus.register(MarkPaymentWithStripeAsSucceeded, OnMarkPaymentWithStripeAsSucceeded.new(event_store))
    end
  end
end
