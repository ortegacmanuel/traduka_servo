Rails.application.routes.draw do
  mount RailsEventStore::Browser => '/res' if Rails.env.development?
  devise_for :users

  get "submit_request_to_join", to: "submit_request_to_join#new"
  post "submit_request_to_join", to: "submit_request_to_join#create"

  post "set_price_per_word", to: "set_price_per_word#create"

  resource :dashboard, only: %i[show]
  namespace :requesters do
    resource :dashboard, only: %i[show]
  end
  namespace :esperantists do
    resource :dashboard, only: %i[show]
  end
  namespace :translators do
    resource :dashboard, only: %i[show]
  end
  namespace :admins do
    resource :dashboard, only: %i[show]
    resources :requests_to_join, only: %i[index show]
  end

  resources :translation_requests, only: %i[new create show] do
    resource :billing_address, only: %i[new create], module: :translation_requests
    resource :payments, only: %i[new create], module: :translation_requests
    namespace :stripe do
      resource :complete, only: %i[show]
    end
    resource :accepted, only: %i[show], module: :translation_requests
  end

  root "home#index"
end