require "rails_event_store"
require "aggregate_root"
require "arkency/command_bus"

Rails.configuration.to_prepare do
  Rails.configuration.event_store = RailsEventStore::JSONClient.new
  Rails.configuration.command_bus = Arkency::CommandBus.new

  AggregateRoot.configure do |config|
    config.default_event_store = Rails.configuration.event_store
  end

  Rails.configuration.event_store.tap do |store|
    store.subscribe_to_all_events(RailsEventStore::LinkByEventType.new)
    store.subscribe_to_all_events(RailsEventStore::LinkByCorrelationId.new)
    store.subscribe_to_all_events(RailsEventStore::LinkByCausationId.new)

    # Read Models
    PriceSettings::Configuration.new.call(store)
    TranslationRequests::Configuration.new.call(store)
    PaymentList::Configuration.new.call(store)

    # Processors
    PricingProcessor::Configuration.new(
      price_settings: PriceSettings::PriceSetting,
      translation_requests: TranslationRequests::TranslationRequest
    ).call(store, Rails.configuration.command_bus)
    PaymentProcessor::Configuration.new(
      payments: PaymentList::Payment
    ).call(store, Rails.configuration.command_bus)
    TranslationRequestAcceptanceProcessor::Configuration.new(
      payments: PaymentList::Payment
    ).call(
      store,
      Rails.configuration.command_bus
    )
  end

  Rails.configuration.command_bus.tap do |bus|
    Requesting::Configuration.new(WordCounter).call(bus, Rails.configuration.event_store)
    Pricing::Configuration.new.call(bus, Rails.configuration.event_store)
    Billing::Configuration.new.call(bus, Rails.configuration.event_store)
    Payments::Configuration.new.call(bus, Rails.configuration.event_store)
  end
end

require 'rails_event_store'
require 'aggregate_root'
require 'arkency/command_bus'

