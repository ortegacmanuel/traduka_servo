class RequestToJoinSubmitted < Event
  attribute :esperantist_id, Types::UUID
  attribute :name, Types::String
  attribute :native_language, Types::String
  attribute :esperanto_level, Types::String
end
