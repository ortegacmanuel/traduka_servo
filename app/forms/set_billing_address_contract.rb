class SetBillingAddressContract < Dry::Validation::Contract
  config.messages.namespace = :set_billing_address
  config.messages.backend = :i18n

  params do
    required(:line1).filled(:string)
    required(:city).filled(:string)
    required(:country).filled(:string)
    optional(:line2).value(:string)
    optional(:postal_code).value(:string)
    optional(:state).value(:string)
  end
end
