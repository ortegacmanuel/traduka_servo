class SetPricePerWordForm < Dry::Struct
  attr_reader :errors

  module Types
    include Dry::Types(default: :nominal)
  end

  attribute :price_per_word, Types::Strict::Float

  def valid?
    attributes = to_hash
    schema = SetPricePerWordContract.new.call(attributes)
    @errors = schema.errors(locale: I18n.locale).to_h.values.flatten
    return false unless errors.empty?

    true
  end
end
