class SubmitRequestToJoinForm
  include ActiveModel::Model
  include ActiveModel::Attributes
  include ActiveModel::Validations

  attribute :name, :string
  attribute :native_language, :string
  attribute :esperanto_level, :string

  validates :name, presence: true
  validates :native_language, 
    inclusion: { in: I18nData.languages.map { |code, _name| code } }
  validates :esperanto_level, 
    inclusion: { in: ["A1", "A2", "B1", "B2", "C1", "C2"] }
end