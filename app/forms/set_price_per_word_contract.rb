class SetPricePerWordContract < Dry::Validation::Contract
  config.messages.namespace = :request_translation
  config.messages.backend = :i18n

  params do
    required(:price_per_word).filled(:float)
  end
end
