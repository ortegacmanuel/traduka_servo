class RequestTranslationContract < Dry::Validation::Contract
  config.messages.namespace = :request_translation
  config.messages.backend = :i18n

  params do
    required(:source_languague).filled(:string)
    required(:target_languague).filled(:string)
    required(:translation_text).filled(:string)
  end

  rule(:source_languague, :target_languague) do
    if values[:source_languague] == values[:target_languague]
      key.failure(I18n.t('dry_validation.errors.request_translation.same_source_and_target'))
    end
  end
end
