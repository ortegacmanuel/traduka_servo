class RequestTranslationForm < Dry::Struct
  attr_reader :errors

  module Types
    include Dry::Types(default: :nominal)
  end

  attribute :source_languague, Types::String
  attribute :target_languague, Types::String
  attribute :translation_text, Types::String

  def valid?
    attributes = to_hash
    schema = RequestTranslationContract.new.call(attributes)
    @errors = schema.errors(locale: I18n.locale).to_h.values.flatten
    return false unless errors.empty?

    true
  end
end
