class SetBillingAddressForm < Dry::Struct
  attr_reader :errors

  module Types
    include Dry::Types(default: :nominal)
  end

  attribute :line1, Types::Strict::String
  attribute :line2, Types::Strict::String
  attribute :city, Types::Strict::String
  attribute :postal_code, Types::Strict::String
  attribute :state, Types::Strict::String
  attribute :country, Types::Strict::String

  def valid?
    attributes = to_hash
    schema = SetBillingAddressContract.new.call(attributes)
    @errors = schema.errors(locale: I18n.locale).to_h.values.flatten
    return false unless errors.empty?

    true
  end
end
