module TranslationRequestAcceptanceProcessor
  class OnTranslationRequestSubmitted
    def call(event)
      TodoList.create!(id: event.data[:translation_request_id])
    end
  end
end