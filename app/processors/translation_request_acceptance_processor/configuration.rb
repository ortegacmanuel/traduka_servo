module TranslationRequestAcceptanceProcessor
  class Configuration
    def initialize(payments:)
      @payments = payments
    end

    def call(store, command_bus)
      store.subscribe(OnTranslationRequestSubmitted, to: [Requesting::TranslationRequestSubmitted])

      store.subscribe(OnPaymentWithStripeSucceeded.new(payments: @payments, command_bus: command_bus), to: [Payments::PaymentWithStripeSucceeded])
    end
  end
end