module TranslationRequestAcceptanceProcessor
  class Processor
    def initialize(command_bus:, payments:)
      @command_bus = command_bus
      @payments = payments
    end

    def call(todo_list)
      return unless todo_list.paid?

      @command_bus.(
        Requesting::AcceptTranslationRequest.new(
          translation_request_id: todo_list.id
        )
      )
    end
  end
end