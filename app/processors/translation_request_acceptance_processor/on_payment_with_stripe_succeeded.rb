module TranslationRequestAcceptanceProcessor
  class OnPaymentWithStripeSucceeded

    def initialize(payments:, command_bus:)
      @payments = payments
      @command_bus = command_bus
    end

    def call(event)
      payment = @payments.find(event.data[:payment_id])
      todo_list = TodoList.find(payment.translation_request_id)

      todo_list.paid = true
      todo_list.save!
      Processor.new(
        command_bus: @command_bus,
        payments: @payments
      ).call(todo_list)
    end
  end
end