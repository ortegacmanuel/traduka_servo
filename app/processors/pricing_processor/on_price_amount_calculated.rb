module PricingProcessor
  class OnPriceAmountCalculated

    def initialize(price_settings:, translation_requests:, command_bus:)
      @price_settings = price_settings
      @translation_requests = translation_requests
      @command_bus = command_bus
    end

    def call(event)
      todo_list = TodoList.find(event.data[:translation_request_id])
      todo_list.amount_calculated = true
      todo_list.save!
      Processor.new(
        command_bus: @command_bus,
        price_settings: @price_settings,
        translation_requests: @translation_requests
      ).call(todo_list)
    end
  end
end