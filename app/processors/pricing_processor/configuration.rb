module PricingProcessor
  class Configuration

    def initialize(price_settings:, translation_requests:)
      @price_settings = price_settings
      @translation_requests = translation_requests
    end

    def call(store, command_bus)
      store.subscribe(OnTranslationRequestSubmitted.new(price_settings: @price_settings, translation_requests: @translation_requests, command_bus: command_bus), to: [Requesting::TranslationRequestSubmitted])
      store.subscribe(OnPriceAmountCalculated.new(price_settings: @price_settings, translation_requests: @translation_requests, command_bus: command_bus), to: [Pricing::PriceAmountCalculated])
      store.subscribe(OnBillingAddressOfTranslationRequestSet.new(price_settings: @price_settings, translation_requests: @translation_requests, command_bus: command_bus), to: [Billing::BillingAddressOfTranslationRequestSet])
      store.subscribe(OnTaxAmountAddedToPrice, to: [Pricing::TaxAmountAddedToPrice])
    end
  end
end