module PricingProcessor
  class Processor
    def initialize(command_bus:, price_settings:, translation_requests:)
      @price_settings = price_settings
      @command_bus = command_bus
      @translation_requests = translation_requests
    end

    def call(todo_list)
      if todo_list.missing_price_amount?
        translation_request = @translation_requests.find_by_id(todo_list.id)
        price_setting = @price_settings.last
        word_count = translation_request&.word_count || todo_list.word_count
        translation_request_id = translation_request&.id || todo_list.id

        if word_count && price_setting&.price_per_word
          @command_bus.(
            Pricing::CalculatePriceAmount.new(
              translation_request_id: translation_request_id  ,
              word_count: word_count,
              price_per_word: price_setting.price_per_word
            )
          )
        end
      elsif todo_list.ready_for_tax_calculation?
        translation_request = @translation_requests.find_by_id(todo_list.id)

        calculation = Stripe::Tax::Calculation.create({
          currency: 'eur',
          customer_details: {
            address: {
              line1: todo_list.billing_address_line1,
              line2: todo_list.billing_address_line2,
              postal_code: todo_list.billing_address_postal_code,
              state: todo_list.billing_address_state,
              country: todo_list.billing_address_country,
            },
            address_source: 'billing',
          },
          line_items: [
            {
              amount: translation_request.amount_in_cents,
              reference: "Translation request #{translation_request.id}",
            },
          ],
        })

        calculation.tax_breakdown.each do |tax|
          @command_bus.(
            Pricing::AddTaxAmountToPrice.new(
              translation_request_id: translation_request.id,
              amount: (tax.amount * 100).to_f,
              percentage: tax.tax_rate_details.percentage_decimal.to_f,
              type: tax.tax_rate_details.tax_type
            )
          )
        end
      end
    end
  end
end