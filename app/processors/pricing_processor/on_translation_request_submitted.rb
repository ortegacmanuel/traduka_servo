module PricingProcessor
  class OnTranslationRequestSubmitted

    def initialize(price_settings:, translation_requests:, command_bus:)
      @price_settings = price_settings
      @translation_requests = translation_requests
      @command_bus = command_bus
    end

    def call(event)
      todo_list = TodoList.create!(
        id: event.data[:translation_request_id],
        word_count: event.data[:translation_text][:word_count]
      )
      Processor.new(
        command_bus: @command_bus,
        price_settings: @price_settings,
        translation_requests: @translation_requests
      ).call(todo_list)
    end
  end
end