module PricingProcessor
  class OnBillingAddressOfTranslationRequestSet

    def initialize(price_settings:, translation_requests:, command_bus:)
      @price_settings = price_settings
      @translation_requests = translation_requests
      @command_bus = command_bus
    end

    def call(event)
      todo_list = TodoList.find(event.data[:translation_request_id])
      todo_list.billing_address_line1 = event.data[:line1]
      todo_list.billing_address_line2 = event.data[:line2]
      todo_list.billing_address_city = event.data[:city]
      todo_list.billing_address_state = event.data[:state]
      todo_list.billing_address_country = event.data[:country]
      todo_list.billing_address_set = true
      todo_list.save!
      Processor.new(
        command_bus: @command_bus,
        price_settings: @price_settings,
        translation_requests: @translation_requests
      ).call(todo_list)
    end
  end
end