module PricingProcessor
  class TodoList < ActiveRecord::Base
    self.table_name = "pricing_todo_lists"

    def missing_price_amount?
      !amount_calculated
    end

    def ready_for_tax_calculation?
      amount_calculated && billing_address_set
    end
  end
end