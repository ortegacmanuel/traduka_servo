module PricingProcessor
  class OnTaxAmountAddedToPrice
    def call(event)
      todo_list = TodoList.find(event.data[:translation_request_id])
      todo_list.tax_calculation_done = true
      todo_list.save!
    end
  end
end