module PaymentProcessor
  class Processor
    def initialize(command_bus:, payments:)
      @command_bus = command_bus
      @payments = payments
    end

    def call(payment)
      return if payment.processed

      amount = (payment.amount * 100).to_i
      return_url = Rails.application.routes.url_helpers.translation_request_stripe_complete_url(payment.translation_request_id)
      payment_intent = Stripe::PaymentIntent.create({
        confirm: true,
        amount: amount,
        currency: 'eur',
        payment_method_types: ['card'],
        payment_method: payment.payment_method_id,
        return_url: return_url,
        use_stripe_sdk: true,
        mandate_data: {
          customer_acceptance: {
            type: "online",
            online: {
              ip_address: payment.customer_ip,
              user_agent: payment.customer_user_agent,
            },
          },
        },
      });

      if payment_intent.status == "succeeded"
        @command_bus.(
          Payments::MarkPaymentWithStripeAsSucceeded.new(
            payment_id: payment.payment_id,
            payment_intent_id: payment_intent.id,
            client_secret: payment_intent.client_secret
          )
        )
      end
    end
  end
end