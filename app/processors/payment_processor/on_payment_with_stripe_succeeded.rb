module PaymentProcessor
  class OnPaymentWithStripeSucceeded
    def call(event)
      payment = Payment.find_by_payment_id(event.data[:payment_id])
      payment.processed = true
      payment.save!
    end
  end
end