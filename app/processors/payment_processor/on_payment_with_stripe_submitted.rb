module PaymentProcessor
  class OnPaymentWithStripeSubmitted

    def initialize(payments:, command_bus:)
      @payments = payments
      @command_bus = command_bus
    end

    def call(event)
      payment = Payment.create!(
        payment_id: event.data[:payment_id],
        payment_method_id: event.data[:payment_method_id],
        translation_request_id: event.data[:translation_request_id],
        amount: event.data[:amount],
        customer_ip: event.data[:customer_ip],
        customer_user_agent: event.data[:customer_user_agent]
      )
      Processor.new(
        command_bus: @command_bus,
        payments: @payments
      ).call(payment)
    end
  end
end