module PaymentProcessor
  class Configuration

    def initialize(payments:)
      @payments = payments
    end

    def call(store, command_bus)
      store.subscribe(OnPaymentWithStripeSubmitted.new(payments: @payments, command_bus: command_bus), to: [Payments::PaymentWithStripeSubmitted])
      store.subscribe(OnPaymentWithStripeSucceeded, to: [Payments::PaymentWithStripeSucceeded])
    end
  end
end