module PaymentList
  class Configuration
    def call(store)
      store.subscribe(OnPaymentWithStripeSubmitted, to: [Payments::PaymentWithStripeSubmitted])
      store.subscribe(OnPaymentWithStripeSucceeded, to: [Payments::PaymentWithStripeSucceeded])
    end
  end
end