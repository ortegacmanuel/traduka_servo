module PaymentList
  class OnPaymentWithStripeSubmitted
    def call(event)
      payment = Payment.find_or_initialize_by id: event.data[:payment_id]
      payment.amount = event.data[:amount]
      payment.translation_request_id = event.data[:translation_request_id]
      payment.payment_method_id = event.data[:payment_method_id]
      payment.status ||= "submitted"
      payment.gateway = "stripe"
      payment.save!
    end
  end
end