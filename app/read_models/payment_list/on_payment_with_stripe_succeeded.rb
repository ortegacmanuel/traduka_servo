module PaymentList
  class OnPaymentWithStripeSucceeded
    def call(event)
      payment = Payment.find_or_initialize_by id: event.data[:payment_id]
      payment.payment_intent_id = event.data[:payment_intent_id]
      payment.client_secret = event.data[:client_secret]
      payment.status = "paid"
      payment.save!
    end
  end
end