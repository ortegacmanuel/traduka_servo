require 'ostruct'

class RequestsToJoinSv
  def initialize(events: [])
    @events = events
  end

  def project
    @events.map do |event|
      {
        esperantist_id: event.data[:esperantist_id],
        name: event.data[:name],
        denaska_lingvo: event.data[:native_language],
        esperanta_nivelo: event.data[:esperanto_level],
        status: 'pending'
      }
    end
  end
end