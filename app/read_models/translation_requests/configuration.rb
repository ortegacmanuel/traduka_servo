module TranslationRequests
  class Configuration
    def call(store)
      store.subscribe(OnTranslationRequestSubmitted, to: [ Requesting::TranslationRequestSubmitted])
      store.subscribe(OnPriceAmountCalculated, to: [ Pricing::PriceAmountCalculated])
      store.subscribe(OnTaxAmountAddedToPrice, to: [ Pricing::TaxAmountAddedToPrice])
      store.subscribe(OnTranslationRequestAccepted, to: [ Requesting::TranslationRequestAccepted])
    end
  end
end