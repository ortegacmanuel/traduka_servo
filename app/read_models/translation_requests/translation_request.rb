module TranslationRequests
  class TranslationRequest < ActiveRecord::Base
    self.table_name = "translation_requests"

    def amount_in_cents
      return 0 unless amount.present?

      (amount * 100).to_i
    end
  end
end