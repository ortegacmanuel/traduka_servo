module TranslationRequests
  class OnTaxAmountAddedToPrice
    def call(event)
      translation_request = TranslationRequest.find_or_initialize_by id: event.data[:translation_request_id]
      translation_request.total = event.data[:total]
      translation_request.tax_total_amount = event.data[:tax_total_amount]
      translation_request.taxes << build_tax(event)
      translation_request.status = "total_calculated"
      translation_request.save!
    end

    private

    def build_tax(event)
      {
        amount: event.data[:tax_amount],
        percentage: event.data[:tax_percentage],
        type: event.data[:tax_type]
      }
    end
  end
end