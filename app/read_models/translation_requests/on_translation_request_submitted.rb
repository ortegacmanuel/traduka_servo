module TranslationRequests
  class OnTranslationRequestSubmitted
    def call(event)
      translation_request = TranslationRequest.find_or_initialize_by id: event.data[:translation_request_id]
      translation_request.source_languague = event.data[:source_languague]
      translation_request.target_languague = event.data[:target_languague]
      translation_request.translation_requester_id = event.data[:translation_requester_id]
      translation_request.text = event.data[:translation_text][:raw]
      translation_request.word_count = event.data[:translation_text][:word_count]
      translation_request.status = "sumitted"
      translation_request.save!
    end
  end
end