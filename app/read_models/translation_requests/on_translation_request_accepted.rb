module TranslationRequests
  class OnTranslationRequestAccepted
    def call(event)
      translation_request = TranslationRequest.find(event.data[:translation_request_id])
      translation_request.status = "accepted"
      translation_request.save!
    end
  end
end