module PriceSettings
  class OnPricePerWordSet
    def call(event)
      price_setting = PriceSetting.find_or_initialize_by id: event.data[:price_settings_id]
      price_setting.price_per_word = event.data[:price_per_word]
      price_setting.save!
    end
  end
end