module PriceSettings
  class Configuration
    def call(store)
      store.subscribe(OnPricePerWordSet, to: [Pricing::PricePerWordSet])
    end
  end
end