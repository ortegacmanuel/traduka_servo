class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  before_validation :set_uuid, on: :create

  def requester?
    roles.include?("requester")
  end

  def esperantist?
    roles.include?("esperantist")
  end

  def translator?
    roles.include?("translator")
  end

  def admin?
    roles.include?("admin")
  end

  private

  def set_uuid
    self.uuid ||= SecureRandom.uuid
  end
end