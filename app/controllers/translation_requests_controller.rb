class TranslationRequestsController < ApplicationController
  layout "dashboard"
  before_action :authenticate_user!

  def new
    @form = RequestTranslationForm.new({source_languague: "", target_languague: "", translation_text: ""})
  end

  def show
    @translation_request = TranslationRequests::TranslationRequest.find(params[:id])

    redirect_to_new_billing_address_path and return if @translation_request.status == "amount_calculated"
    redirect_to_new_payment_path and return if @translation_request.status == "total_calculated"
    redirect_to_accepted_translation_request_path and return if @translation_request.status == "accepted"
  end

  def create
    @form = RequestTranslationForm.new(
      source_languague: params[:source_languague],
      target_languague: params[:target_languague],
      translation_text: params[:translation_text]
    )
    render_errors and return unless @form.valid?

    translation_request_id = SecureRandom.uuid
    translation_requester_id = current_user.uuid
    command_bus.(
      Requesting::SubmitTranslationRequest.new(
        translation_request_id: translation_request_id,
        translation_requester_id: translation_requester_id,
        source_languague: @form.source_languague,
        target_languague: @form.target_languague,
        translation_text: {
          raw: @form.translation_text,
          word_count: nil
        }
      )
    )

    redirect_to translation_request_path(translation_request_id), status: :see_other
  rescue Requesting::TranslationRequest::MaxNumberOfWordsExceeded
    flash[:notice] = "Max number of words exceeded"
    render :new, status: :unprocessable_entity
  end

  private

  def redirect_to_new_billing_address_path
    redirect_to new_translation_request_billing_address_path(@translation_request.id)
  end

  def redirect_to_new_payment_path
    redirect_to new_translation_request_payments_path(@translation_request.id)
  end

  def redirect_to_accepted_translation_request_path
    redirect_to translation_request_accepted_path(@translation_request.id)
  end

  def render_errors
    render :new, status: :unprocessable_entity
  end
end