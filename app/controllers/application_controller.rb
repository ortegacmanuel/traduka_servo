class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?

  def event_store
    Rails.configuration.event_store
  end

  def command_bus
    Rails.configuration.command_bus
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up) do |user_params|
      user_params.permit({ roles: [] }, :email, :password, :password_confirmation, :uuid)
    end
  end

  def after_sign_up_path_for(resource)
    if resource.admin?
      admins_dashboard_path
    elsif resource.requester?
      requesters_dashboard_path
    elsif resource.esperantist?
      esperantists_dashboard_path
    elsif resource.translator?
      translators_dashboard_path
    else
      root_path
    end
  end

  def after_sign_in_path_for(resource)
    if resource.admin?
      admins_dashboard_path
    elsif resource.requester?
      requesters_dashboard_path
    elsif resource.esperantist?
      esperantists_dashboard_path
    elsif resource.translator?
      translators_dashboard_path
    else
      root_path
    end
  end
end