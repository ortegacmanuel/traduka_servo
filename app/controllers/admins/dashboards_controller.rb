module Admins
  class DashboardsController < ApplicationController
    layout "dashboard"
    before_action :authenticate_user!

    def show
      @price_setting = PriceSettings::PriceSetting.first
    end
  end
end
