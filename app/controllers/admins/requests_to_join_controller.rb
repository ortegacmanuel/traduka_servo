module Admins
  class RequestsToJoinController < ApplicationController
    layout "dashboard"
    before_action :authenticate_user!

    def index
      @requests_to_join = RequestsToJoinSv.new(
        events: event_store.read.of_type([RequestToJoinSubmitted]).to_a
      ).project
    end

    def show
      @requests_to_join = RequestsToJoinSv.new(
        events: event_store.read.of_type([RequestToJoinSubmitted]).to_a
      ).project
      @request_to_join = @requests_to_join.find { |request| request[:esperantist_id] == params[:id] }
    end
  end
end
