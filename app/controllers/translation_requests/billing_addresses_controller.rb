module TranslationRequests
  class BillingAddressesController < ApplicationController
    layout "dashboard"
    before_action :authenticate_user!

    def new
      @translation_request = TranslationRequests::TranslationRequest.find(params[:translation_request_id])
      @translation_requester = TranslationRequesters::TranslationRequester.find(current_user.uuid)
    end

    def create
      @form = SetBillingAddressForm.new(
        line1: params[:line1],
        line2: params[:line2],
        city: params[:city],
        postal_code: params[:postal_code],
        state: params[:state],
        country: params[:country]
      )
      render_errors and return unless @form.valid?

      @translation_request = TranslationRequests::TranslationRequest.find(params[:translation_request_id])
      command_bus.(
        Billing::SetBillingAddressOfTranslationRequest.new(
          translation_request_id: @translation_request.id,
          line1: @form.line1,
          line2: @form.line2,
          city: @form.city,
          postal_code: @form.postal_code,
          state: @form.state,
          country: @form.country,
          set_as_default: false
        )
      )

      redirect_to translation_request_path(@translation_request.id)
    end

    private

    def render_errors
      render :new, status: :unprocessable_entity
    end
  end
end