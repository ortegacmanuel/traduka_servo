module TranslationRequests
  class AcceptedsController < ApplicationController
    layout "dashboard"
    before_action :authenticate_user!

    def show
      @translation_request = TranslationRequests::TranslationRequest.find(params[:translation_request_id])
    end
  end
end