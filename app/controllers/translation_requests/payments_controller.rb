module TranslationRequests
  class PaymentsController < ApplicationController
    layout "dashboard"
    before_action :authenticate_user!

    def new
      @translation_request = TranslationRequests::TranslationRequest.find(params[:translation_request_id])
      @translation_requester = TranslationRequesters::TranslationRequester.find(current_user.uuid)
    end

    def create
      @translation_request = TranslationRequests::TranslationRequest.find(params[:translation_request_id])
      payment_id = SecureRandom.uuid

      command_bus.(
        Payments::SubmitPaymentWithStripe.new(
          payment_id: payment_id,
          translation_request_id: @translation_request.id,
          payment_method_id: params[:payment_method_id],
          amount: @translation_request.total,
          customer_ip: request.ip,
          customer_user_agent: request.user_agent
        )
      )

      render json: { redirectUrl: translation_request_path(@translation_request) }
    end

    private

    def render_errors
      render :new, status: :unprocessable_entity
    end
  end
end