class SubmitRequestToJoinController < ApplicationController
  layout "dashboard"

  def new
    @submit_request_to_join_form = SubmitRequestToJoinForm.new(name: "", native_language: "", esperanto_level: "")
    unless user_signed_in?
      store_location_for(:user, submit_request_to_join_path)
      flash[:alert] = "Vi devas krei konton por sendi aliĝo-peton"
      redirect_to new_user_registration_path(roles: ["esperantist"])
    end
  end

  def create
    @submit_request_to_join_form = SubmitRequestToJoinForm.new(
      name: params[:submit_request_to_join_form][:name],
      native_language: params[:submit_request_to_join_form][:native_language],
      esperanto_level: params[:submit_request_to_join_form][:esperanto_level]
    )
    unless @submit_request_to_join_form.valid?
      render :new, status: :unprocessable_entity
      return
    end

    events = event_store.read.of_type([RequestToJoinSubmitted])

    result = SubmitRequestToJoin.new(
      esperantist_id: current_user.uuid,
      name: @submit_request_to_join_form.name,
      esperanto_level: @submit_request_to_join_form.esperanto_level,
      native_language: @submit_request_to_join_form.native_language
    ).handle(events: events.to_a)

    if result.success?
      event_store.publish(result.events)
      
      flash[:notice] = "Aliĝo-peto sendita"
      redirect_to esperantists_dashboard_path
    else
      flash[:alert] = result.error
      render :new, status: :unprocessable_entity
    end
  end
end