module Esperantists
  class DashboardsController < ApplicationController
    layout "dashboard"
    before_action :authenticate_user!

    def show
      events = event_store.read.of_type([RequestToJoinSubmitted]).to_a
      requests_to_join_sv = RequestsToJoinSv.new(events: events).project
      @request_to_join_sv = requests_to_join_sv.find { |request| request[:esperantist_id] == current_user.uuid }
    end
  end
end
