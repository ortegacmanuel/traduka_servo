class SetPricePerWordController < ApplicationController
  layout "dashboard"
  before_action :authenticate_user!

  def create
    form = SetPricePerWordForm.new(
      price_per_word: params[:price_per_word].to_f
    )
    return redirect_to dashboard_path, notice: "Validation went wrong" unless form.valid?

    price_setting = PriceSettings::PriceSetting.first
    price_settings_id = price_setting&.id || SecureRandom.uuid
    command_bus.(
      Pricing::SetPricePerWord.new(
        price_settings_id: price_settings_id,
        price_per_word: form.price_per_word
      )
    )

    redirect_to dashboard_path, notice: "Price per word set successfully"
  end

  private

  def render_errors
    render :new, status: :unprocessable_entity
  end
end