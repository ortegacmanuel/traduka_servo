class DashboardsController < ApplicationController
  layout "dashboard"
  before_action :authenticate_user!

  def show
    if current_user.admin?
      redirect_to admins_dashboard_path
    elsif current_user.requester?
      redirect_to requesters_dashboard_path
    elsif current_user.esperantist?
      redirect_to esperantists_dashboard_path
    elsif current_user.translator?
      redirect_to translators_dashboard_path
    else
      redirect_to root_path
    end
  end
end