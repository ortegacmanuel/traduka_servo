// Entry point for the build script in your package.json
import "@hotwired/turbo-rails"
import "./controllers"

import "preline";

document.addEventListener("turbo:load", () => {
  window.HSStaticMethods.autoInit();
  window.HSAccordion.autoInit();
  window.HSDropdown.autoInit();
  window.HSOverlay.autoInit();
  window.HSSelect.autoInit();
});