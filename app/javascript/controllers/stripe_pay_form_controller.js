import { Controller } from "@hotwired/stimulus";
import { loadStripe } from "@stripe/stripe-js";

export default class extends Controller {
  static values = {
    publicKey: String,
    submitPaymentPath: String,
    buttonText: String,
    customerEmail: String,
  };
  static targets = ["errorContainer", "submit", "form", "paymentMethod"];

  async connect() {
    this.stripe = await loadStripe(this.publicKeyValue);
    this.elements = this.stripe.elements();

    this.card = this.elements.create("card");
    this.card.mount("#card-element");
  }

  async submit(event) {
    event.preventDefault();

    this.submitTarget.disabled = true;

    this.submitTarget.innerHTML = "Processing...";

    const elements = this.elements;
    // Trigger form validation and wallet collection
    const {error: submitError} = await elements.submit();
    if (submitError) {
      this.handleError(submitError);
      return;
    }

    // Create the PaymentMethod using the details collected by the Payment Element
    const {error, paymentMethod} = await this.stripe.createPaymentMethod({
      elements,
      params: {
        billing_details: {
          email: this.customerEmail,
        }
      }
    });

    if (error) {
      // This point is only reached if there's an immediate error when
      // creating the PaymentMethod. Show the error to your customer (for example, payment details incomplete)
      this.handleError(error);
      return;
    }

    // Create the PaymentIntent
    const res = await fetch(this.submitPaymentPathValue, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "X-CSRF-Token": this.getMetaValue("csrf-token")
      },
      body: JSON.stringify({
        payment_method_id: paymentMethod.id,
      }),
    });

    const data = await res.json();

    // Handle any next actions or errors. See the Handle any next actions step for implementation.
    this.handleServerResponse(data);
  }

  handleError(error) {
    this.errorContainerTarget.innerHTML = error.message;
    this.errorContainerTarget.classList.remove("hidden");
    this.submitTarget.disabled = false;
    this.submitTarget.innerHTML = this.buttonTextValue;
    this.submitTarget.classList.remove("bg-gray-100");
  }

  async handleServerResponse(response){
    if (response.error) {
      // Show error from server on payment form
    } else if (response.status === "requires_action") {
      // Use Stripe.js to handle the required next action
      const {
        error,
        paymentIntent
      } = await stripe.handleNextAction({
        clientSecret: response.clientSecret
      });


      if (error) {
        // Show error from Stripe.js in payment form
      } else {
        // Actions handled, show success message
      }
    } else {
      // No actions needed, show success message
      window.location.href = response.redirectUrl;
    }
  }

  getMetaValue(name) {
    const element = document.head.querySelector(`meta[name="${name}"]`)
    return element.getAttribute("content")
  }
}