import { Controller } from "@hotwired/stimulus"

export default class extends Controller {
  connect() {
    this.updateDarkMode();
    window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', this.updateDarkMode.bind(this));
  }

  updateDarkMode() {
    if (window.matchMedia('(prefers-color-scheme: dark)').matches) {
      document.documentElement.classList.add('dark');
    } else {
      document.documentElement.classList.remove('dark');
    }
  }
}