class SubmitRequestToJoin < Command

  attribute :esperantist_id, Types::UUID
  attribute :name, Types::String
  attribute :native_language, Types::String
  attribute :esperanto_level, Types::String

  def handle(events: [])
    if events.any? { |event| event.is_a?(RequestToJoinSubmitted) && event.data[:esperantist_id] == esperantist_id }
      return Result.new(false, error: "Request to join already submitted")
    end

    event = RequestToJoinSubmitted.new(
      data: {
        esperantist_id: esperantist_id,
        name: name,
        native_language: native_language,
        esperanto_level: esperanto_level
      }
    )

    Result.new(true, events: [event])
  end
end