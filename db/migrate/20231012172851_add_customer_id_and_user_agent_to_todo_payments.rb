class AddCustomerIdAndUserAgentToTodoPayments < ActiveRecord::Migration[7.0]
  def change
    add_column :todo_payments, :customer_ip, :string
    add_column :todo_payments, :customer_user_agent, :string
  end
end
