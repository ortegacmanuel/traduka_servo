class CreatePricingTodoLists < ActiveRecord::Migration[7.0]
  def change
    create_table :pricing_todo_lists, id: false do |t|
      t.uuid :id, primary_key: true
      t.boolean :amount_calculated, default: false
      t.boolean :vat_added, default: false

      t.timestamps
    end
  end
end
