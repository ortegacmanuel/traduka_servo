class CreateEsperantists < ActiveRecord::Migration[7.0]
  def change
    create_table :esperantists, id: false do |t|
      t.uuid :id, primary_key: true
      t.string :email

      t.timestamps
    end
  end
end
