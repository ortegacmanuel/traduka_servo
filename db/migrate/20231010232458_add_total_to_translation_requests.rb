class AddTotalToTranslationRequests < ActiveRecord::Migration[7.0]
  def change
    add_column :translation_requests, :total, :float
  end
end
