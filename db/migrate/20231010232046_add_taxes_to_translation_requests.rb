class AddTaxesToTranslationRequests < ActiveRecord::Migration[7.0]
  def change
    add_column :translation_requests, :taxes, :jsonb, array: true, default: []
  end
end
