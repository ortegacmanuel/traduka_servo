class CreateTodoUsersToProfiles < ActiveRecord::Migration[7.0]
  def change
    create_table :todo_users_to_profiles do |t|
      t.string :user_id, null: false
      t.string :role, null: false
      t.boolean :done, null: false, default: false

      t.timestamps
    end
  end
end
