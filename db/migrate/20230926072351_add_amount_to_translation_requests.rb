class AddAmountToTranslationRequests < ActiveRecord::Migration[7.0]
  def change
    add_column :translation_requests, :amount, :float
  end
end
