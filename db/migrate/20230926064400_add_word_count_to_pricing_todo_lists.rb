class AddWordCountToPricingTodoLists < ActiveRecord::Migration[7.0]
  def change
    add_column :pricing_todo_lists, :word_count, :integer
  end
end
