class RemoveAllProfilesRelatedTables < ActiveRecord::Migration[7.0]
  def change
    drop_table :esperantists
    drop_table :translators
    drop_table :translation_requesters
    drop_table :todo_users_to_profiles
  end
end
