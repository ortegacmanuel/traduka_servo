class CreatePayments < ActiveRecord::Migration[7.0]
  def change
    create_table :payments, id: false do |t|
      t.uuid :id, primary_key: true
      t.uuid :translation_request_id
      t.float :amount
      t.string :payment_method_id
      t.string :status
      t.string :gateway

      t.timestamps
    end
  end
end
