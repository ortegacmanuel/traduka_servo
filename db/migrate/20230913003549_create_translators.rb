class CreateTranslators < ActiveRecord::Migration[7.0]
  def change
    create_table :translators, id: false do |t|
      t.uuid :id, primary_key: true
      t.string :email

      t.timestamps
    end
  end
end
