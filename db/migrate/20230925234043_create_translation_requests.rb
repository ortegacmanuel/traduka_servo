class CreateTranslationRequests < ActiveRecord::Migration[7.0]
  def change
    create_table :translation_requests, id: false do |t|
      t.uuid :id, primary_key: true
      t.string :source_languague
      t.string :target_languague
      t.uuid :translation_requester_id
      t.text :text
      t.integer :word_count

      t.timestamps
    end
  end
end
