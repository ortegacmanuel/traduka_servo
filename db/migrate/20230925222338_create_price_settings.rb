class CreatePriceSettings < ActiveRecord::Migration[7.0]
  def change
    create_table :price_settings, id: false do |t|
      t.uuid :id, primary_key: true
      t.float :price_per_word

      t.timestamps
    end
  end
end
