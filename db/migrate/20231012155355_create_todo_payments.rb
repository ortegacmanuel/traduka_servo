class CreateTodoPayments < ActiveRecord::Migration[7.0]
  def change
    create_table :todo_payments do |t|
      t.uuid :payment_id
      t.float :amount
      t.string :payment_method_id
      t.boolean :processed, null: false, default: false

      t.timestamps
    end
  end
end
