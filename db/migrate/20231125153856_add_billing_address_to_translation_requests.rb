class AddBillingAddressToTranslationRequests < ActiveRecord::Migration[7.0]
  def change
    add_column :translation_requests, :billing_address_line1, :string
    add_column :translation_requests, :billing_address_line2, :string
    add_column :translation_requests, :billing_address_city, :string
    add_column :translation_requests, :billing_address_state, :string
    add_column :translation_requests, :billing_address_postal_code, :string
    add_column :translation_requests, :billing_address_country, :string
  end
end
