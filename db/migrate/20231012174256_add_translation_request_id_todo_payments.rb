class AddTranslationRequestIdTodoPayments < ActiveRecord::Migration[7.0]
  def change
    add_column :todo_payments, :translation_request_id, :uuid
  end
end
