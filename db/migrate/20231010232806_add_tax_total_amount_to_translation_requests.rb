class AddTaxTotalAmountToTranslationRequests < ActiveRecord::Migration[7.0]
  def change
    add_column :translation_requests, :tax_total_amount, :float
  end
end
