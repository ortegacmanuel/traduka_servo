class RenameVatAddedToTaxCalculationDoneToPricingTodoLists < ActiveRecord::Migration[7.0]
  def change
    rename_column :pricing_todo_lists, :vat_added, :tax_calculation_done
  end
end
