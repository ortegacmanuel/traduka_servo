class AddStatusToTranslationRequests < ActiveRecord::Migration[7.0]
  def change
    add_column :translation_requests, :status, :string
  end
end
