class CreateTranslationRequestAcceptanceTodoLists < ActiveRecord::Migration[7.0]
  def change
    create_table :translation_request_acceptance_todo_lists,id: false do |t|
      t.uuid :id, primary_key: true
      t.boolean :paid, default: false

      t.timestamps
    end
  end
end
