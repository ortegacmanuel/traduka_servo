module Pricing
  class Configuration
    def initialize; end

    def call(bus, event_store)
      bus.register(SetPricePerWord, OnSetPricePerWord.new(event_store))
      bus.register(CalculatePriceAmount, OnCalculatePriceAmount.new(event_store))
      bus.register(AddTaxAmountToPrice, OnAddTaxAmountToPrice.new(event_store))
    end
  end
end
