module Pricing
  class AddTaxAmountToPrice < Command
    attribute :translation_request_id, Types::UUID
    attribute :amount, Types::Float
    attribute :percentage, Types::Float
    attribute :type, Types::String

    alias :aggregate_id :translation_request_id
  end
end