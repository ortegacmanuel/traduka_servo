module Pricing
  class PriceSettings
    include AggregateRoot

    def initialize(id)
      @id = id
    end

    def set_price_per_word(price_per_word)
      apply PricePerWordSet.new(data: {
        price_settings_id: @id,
        price_per_word: price_per_word
      })
    end

    on PricePerWordSet do |event|
      @price_per_word = event.data[:price_per_word]
    end
  end
end