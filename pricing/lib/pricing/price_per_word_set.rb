module Pricing
  class PricePerWordSet < Event
    attribute :price_settings_id, Types::UUID
    attribute :price_per_word, Types::Strict::Float
  end
end