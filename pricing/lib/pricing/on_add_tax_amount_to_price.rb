module Pricing
  class OnAddTaxAmountToPrice

    def initialize(event_store)
      @repository = AggregateRootRepository.new(event_store)
    end

    def call(command)
      @repository.with_aggregate(Price, command.aggregate_id) do |price|
        price.add_tax_amount(command.amount, command.percentage, command.type)
      end
    end
  end
end