module Pricing
  class TaxAmountAddedToPrice < Event
    attribute :translation_request_id, Types::UUID
    attribute :total, Types::Float
    attribute :tax_amount, Types::Float
    attribute :tax_percentage, Types::Float
    attribute :tax_type, Types::String
    attribute :tax_total_amount, Types::Float
  end
end