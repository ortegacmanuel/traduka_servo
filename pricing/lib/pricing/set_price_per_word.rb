module Pricing
  class SetPricePerWord < Command
    attribute :price_settings_id, Types::UUID
    attribute :price_per_word, Types::Strict::Float

    alias :aggregate_id :price_settings_id
  end
end