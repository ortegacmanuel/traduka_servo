module Pricing
  class PriceAmountCalculated < Event
    attribute :translation_request_id, Types::UUID
    attribute :amount, Types::Float
  end
end