module Pricing
  class Price
    include AggregateRoot

    def initialize(id)
      @id = id
      @taxes = []
    end

    def calculate_amount(word_count, price_per_word)
      amount = (word_count * price_per_word).round(2)

      apply PriceAmountCalculated.new(data: {
        translation_request_id: @id,
        amount: amount
      })
    end

    def add_tax_amount(tax_amount, tax_percentage, tax_type)
      total = @total + tax_amount
      tax_total_amount = @taxes.sum {|tax| tax[:amount] } + tax_amount

      apply TaxAmountAddedToPrice.new(data: {
        translation_request_id: @id,
        total: total,
        tax_amount: tax_amount,
        tax_percentage: tax_percentage,
        tax_type: tax_type,
        tax_total_amount: tax_total_amount
      })
    end

    on PriceAmountCalculated do |event|
      @amount = event.data[:amount]
      @total = event.data[:amount]
    end

    on TaxAmountAddedToPrice do |event|
      @total = event.data[:total]
      tax = {
        amount: event.data[:tax_amount],
        percentage: event.data[:tax_percentage],
        type: event.data[:tax_type]
      }
      @taxes << tax
    end
  end
end