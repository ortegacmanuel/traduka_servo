module Pricing
  class OnSetPricePerWord

    def initialize(event_store)
      @repository = AggregateRootRepository.new(event_store)
    end

    def call(command)
      @repository.with_aggregate(PriceSettings, command.aggregate_id) do |price_settings|
        price_settings.set_price_per_word(command.price_per_word)
      end
    end
  end
end