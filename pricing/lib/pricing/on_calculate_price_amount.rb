module Pricing
  class OnCalculatePriceAmount

    def initialize(event_store)
      @repository = AggregateRootRepository.new(event_store)
    end

    def call(command)
      @repository.with_aggregate(Price, command.aggregate_id) do |price|
        price.calculate_amount(command.word_count, command.price_per_word)
      end
    end
  end
end