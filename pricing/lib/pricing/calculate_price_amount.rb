module Pricing
  class CalculatePriceAmount < Command
    attribute :translation_request_id, Types::UUID
    attribute :word_count, Types::Integer
    attribute :price_per_word, Types::Float

    alias :aggregate_id :translation_request_id
  end
end