ENV["RAILS_ENV"] ||= "test"
require_relative "../config/environment"
require "rails/test_help"

class ActiveSupport::TestCase
  parallelize(workers: :number_of_processors)

  fixtures :all

  def event_to_compare(event)
    { type: event.event_type, data: event.data }
  end

  def assert_events(expected:, new_events:)
    assert_equal expected.map { |ev| event_to_compare(ev) }, new_events.map { |ev| event_to_compare(ev) }
  end
end