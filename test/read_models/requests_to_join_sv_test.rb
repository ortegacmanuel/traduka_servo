require "test_helper"

class RequestsToJoinSvTest < ActiveSupport::TestCase
  test "should project events correctly" do
    events = [
      RequestToJoinSubmitted.new(
        data: {
          esperantist_id: SecureRandom.uuid,
          name: "John Doe",
          native_language: "EN",
          esperanto_level: "A1"
        }
      ),
      RequestToJoinSubmitted.new(
        data: {
          esperantist_id: SecureRandom.uuid,
          name: "Jane Smith",
          native_language: "FR",
          esperanto_level: "B2"
        }
      )
    ]

    result = RequestsToJoinSv.new(events: events).project
    expected = events.map do |event|
      {
        esperantist_id: event.data[:esperantist_id],
        name: event.data[:name],
        denaska_lingvo: event.data[:native_language],
        esperanta_nivelo: event.data[:esperanto_level],
        status: "pending"
      }
    end

    assert_equal expected, result
  end
end