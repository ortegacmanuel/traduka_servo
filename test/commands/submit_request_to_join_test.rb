require "test_helper"

class SubmitRequestToJoinTest < ActiveSupport::TestCase
  test "should handle the command and return a successful result" do
    ## Given
    events = []

    ## When
    command = SubmitRequestToJoin.new(
      esperantist_id: SecureRandom.uuid,
      name: "Antono Gash",
      native_language: "ES",
      esperanto_level: "B2"
    )
    result = command.handle(events: events)

    ## Then
    expected_events = [
      RequestToJoinSubmitted.new(
        esperantist_id: command.esperantist_id,
        esperanto_level: command.esperanto_level,
        native_language: command.native_language,
        name: command.name
      )
    ]

    assert result.success?
    assert_events(expected: expected_events, new_events: result.events)
  end

  test "must prevent duplicate requests" do
    esperantist_id = SecureRandom.uuid

    ## Given
    events = [
      RequestToJoinSubmitted.new(
        data: {
          esperantist_id: esperantist_id,
          name: "Antono Gash",
          native_language: "ES",
          esperanto_level: "B2"
        }
      )
    ]

    ## When
    command = SubmitRequestToJoin.new(
      esperantist_id: esperantist_id,
      name: "Antono Gash",
      native_language: "ES",
      esperanto_level: "B2"
    )
    result = command.handle(events: events)

    ## Then
    assert_not result.success?
    assert_equal "Request to join already submitted", result.error
  end
end