class WordCounter
  def initialize(text)
    @text = text
  end

  def word_count
    @text.split(" ").size
  end
end