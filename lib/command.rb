require 'dry-struct'

class Command < Dry::Struct
  Invalid = Class.new(StandardError)

  def self.new(*)
    super
  rescue Dry::Struct::Error => doh
    raise Invalid, doh
  end

  def handle(events: [])
    raise NotImplementedError, "Subclass #{self.class.name} must implement #handle"
  end

  class Result
    attr_reader :events, :error

    def initialize(success, events: [], error: nil)
      @success = success
      @events = events
      @error = error
    end

    def success?
      @success
    end
  end
  private_constant :Result
end