require 'dry-types'

module Types
  include Dry::Types()

  UUID          = Types::Strict::String.constrained(format: /\A[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-4[0-9a-fA-F]{3}-[89abAB][0-9a-fA-F]{3}-[0-9a-fA-F]{12}\z/i)
  ID            = Types::Strict::Integer
  Metadata      = Types::Hash.schema(timestamp: Types::Params::DateTime.meta(omittable: true))
  TransactionId = Types::Strict::String.constrained(format: /\A[0-9a-fA-F]{32}\z/i)
  Email = Types::String.constrained(format: /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i)

  class TranslationText < Dry::Struct
    attribute :raw, Types::String
    attribute :word_count, Types::Integer.optional
  end
end
